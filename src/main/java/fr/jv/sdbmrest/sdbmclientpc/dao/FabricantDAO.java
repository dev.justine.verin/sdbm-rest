package fr.jv.sdbmrest.sdbmclientpc.dao;




import fr.jv.sdbmrest.sdbmclientpc.metiers.Fabricant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FabricantDAO extends DAO<Fabricant, Fabricant, Integer> {
    @Override
    public Fabricant getById(Integer id) {
        String sqlRequest = "SELECT ID_FABRICANT, NOM_FABRICANT FROM FABRICANT where " +
                "ID_FABRICANT = " + id;

        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if (resultSet.next()) {
                Fabricant fabricant = new Fabricant(resultSet.getInt(1),
                        resultSet.getString(2));
                fabricant.setMarques(DAOFactory.getMarqueDAO().getByFabricant(fabricant));
                return fabricant;
            }

            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    @Override
    public ArrayList<Fabricant> getAll() {
        ArrayList<Fabricant> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_FABRICANT, NOM_FABRICANT from FABRICANT";
        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while (resultSet.next()) {
                Fabricant fabricant = new Fabricant(resultSet.getInt(1),
                        resultSet.getString(2));
                fabricant.setMarques(DAOFactory.getMarqueDAO().getByFabricant(fabricant));
                liste.add(fabricant);
            }
            resultSet.close();

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return liste;

    }

    @Override
    public ArrayList<Fabricant> getLike(Fabricant object) {
        return new ArrayList<>();
    }

    @Override
    public boolean insert(Fabricant object) {
        return false;
    }

    @Override
    public boolean update(Fabricant object) {
        return false;
    }

    @Override
    public boolean delete(Fabricant object) {
        return false;
    }
}