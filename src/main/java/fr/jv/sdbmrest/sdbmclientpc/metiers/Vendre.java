package fr.jv.sdbmrest.sdbmclientpc.metiers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
@Setter
public class Vendre {

    private VendreID vendreID;
    private Integer quantite;
    private BigDecimal prixVente;


}