package fr.jv.sdbmrest;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Couleur;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static io.restassured.RestAssured.given;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CouleursResourceTest {
    static int id;
    @Order(1)
    void dummyTest(){
        given().get("api/hello-world");
    }
    @Test
    @Order(2)
    void getCouleurs() {
        id = 1;
        Couleur[] tableau = given()
                .get("api/couleurs")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(200)
                .extract().as(Couleur[].class);
        List<Couleur> couleurs = Arrays.stream(tableau).toList();
        assert couleurs.contains(DAOFactory.getCouleurDAO().getById(id));
    }

    @Test
    void getCouleurByID() {
        id = 1;
        Couleur couleur = given()
                .get("api/couleurs/" + id)
                .then().contentType(MediaType.APPLICATION_JSON)
                .statusCode(200)
                .extract().as(Couleur.class);
        assert couleur.getId() == id;
    }

    @Test
    void addCouleur() {
        Couleur couleurToAdd = new Couleur(0, "Caramel");
        Couleur couleurAdded = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(couleurToAdd)
                .post("api/couleurs")
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().as(Couleur.class);
        id = couleurAdded.getId();
        assert !(Objects.equals(couleurToAdd.getId(), id));


    }

    @Test
    void deleteCouleur() {
        Couleur couleur = DAOFactory.getCouleurDAO().getById(id);
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(couleur)
                .delete("api/couleurs")
                .then()
                .statusCode(HttpServletResponse.SC_NO_CONTENT);
        assert DAOFactory.getCouleurDAO().getById(id) == null;


    }
}