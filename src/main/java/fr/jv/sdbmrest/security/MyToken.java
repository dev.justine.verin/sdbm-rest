package fr.jv.sdbmrest.security;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Utilisateur;

import java.util.Calendar;
import java.util.Date;


public class MyToken {

    private static final String SECRET_KEY = "MySecretK3Y";

    public static String generate(User user) {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.MINUTE, 10);
        Date expiration = calendar.getTime();
        Utilisateur utilisateur = new Utilisateur(user.getLogin(), user.getPassword());
        String token = JWT.create()
                .withClaim("user", user.getLogin())
                .withClaim("role", utilisateur.getRole())
                .withIssuedAt(now)
                .withExpiresAt(expiration)
                .sign(Algorithm.HMAC256(SECRET_KEY));
        return "Bearer " + token;
    }

    public static boolean validate(String token) {
        if (token.startsWith("Bearer "))
            token = token.substring(7);

        Boolean retour = false;
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET_KEY)).build();

        try {
            DecodedJWT jwt = verifier.verify(token);
            retour = !jwt.getExpiresAt().before(now);
        } catch (Exception e) {

        }
        return retour;
    }

    public static String getRole(String token) {
        if (token != null && token.startsWith("Bearer "))
            token = token.substring(7);
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET_KEY)).build();
        DecodedJWT jwt = verifier.verify(token);
        String role = jwt.getClaim("role").asString();
        if (role != null) {
            return role;
        }
        return "";

    }


}