package fr.jv.sdbmrest.hateoas;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class HateOAS {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Link> links;

    public HateOAS(){
        links = new ArrayList<Link>();

    }

    public void addLink(Link link){
        links.add(link);
    }

}