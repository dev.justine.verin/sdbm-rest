package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
public class Fabricant {
    @NonNull
    private Integer id;
    @NonNull
    private String libelle;
    private ArrayList<Marque> marques;


    public Fabricant(int id, String libelle){
        this.id = id;
        this.libelle = libelle;
        marques = new ArrayList<>();



    }
    public String toString(){
        return libelle;

    }
    
    @Override
    public boolean equals(Object other){
        if(other == this )
            return true;
        if(!(other instanceof Fabricant ))
            return false;
        Fabricant fabricant = (Fabricant) other;
        return fabricant.id == this.id;

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}