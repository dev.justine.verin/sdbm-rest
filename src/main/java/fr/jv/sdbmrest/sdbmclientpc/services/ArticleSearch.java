package fr.jv.sdbmrest.sdbmclientpc.services;


import fr.jv.sdbmrest.sdbmclientpc.metiers.*;
import lombok.Getter;
import lombok.Setter;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;

import java.util.List;


@Getter
@Setter

public class ArticleSearch {

    //un champ par critère de recherche
    private String libelleArticle = "";
    private Couleur couleur;
    private Marque marque;
    private Typebiere typebiere;
    private Fabricant fabricant;
    private Pays pays;
    private Continent continent;
    private Integer volume;
    private float titrageMin;
    private float titrageMax;
    private Integer numeroPage;
    private Integer lignesParPage;
    private Integer nombreLignesTotal;
    private Integer nombrePages;

    public ArticleSearch(){
        couleur = new Couleur(0, "");
        typebiere = new Typebiere(0, "");
        marque = new Marque(0, "", null, null);
        fabricant = new Fabricant(0, "");
        pays = new Pays(0, "", null);
        continent = new Continent(0, "");
        volume = 0;
        titrageMin = (float) 0.5;
        titrageMax =  (float) 40;
        numeroPage = 1;
        lignesParPage = 10;
        nombrePages = 1;
        nombreLignesTotal = 0;


    }

    public void setCouleur(Couleur couleur) {
        if(couleur == null){
            this.couleur = new Couleur(0, "");
            return;
        }
        this.couleur = couleur;
    }

    public void setMarque(Marque marque) {
        if(marque == null){
            this.marque = new Marque(0, "", null, null);
            return;
        }
        this.marque = marque;
    }

    public void setTypebiere(Typebiere typebiere) {
        if(typebiere == null){
            this.typebiere = new Typebiere(0, "");
            return;
        }
        this.typebiere = typebiere;
    }

    public void setFabricant(Fabricant fabricant) {
        if(fabricant == null){
            this.fabricant = new Fabricant(0, "");
            return;
        }
        this.fabricant = fabricant;
    }

    public void setPays(Pays pays) {
        if(pays == null){
            this.pays = new Pays(0, "", null);
            return;
        }
        this.pays = pays;
    }

    public void setContinent(Continent continent) {
        if(continent == null){
            this.continent = new Continent(0, "");
            return;
        }
        this.continent = continent;
    }



    public List<Article> searchArticles(){
        return DAOFactory.getArticleDAO().getLike(this);


    }


    private Integer calculNombrePages() {
        int nombreMinPages = nombreLignesTotal / lignesParPage;
        if(nombreLignesTotal % lignesParPage!= 0){
            return nombreMinPages + 1;
        }
        return nombreMinPages;

    }

    public Integer getNombrePages() {
        return calculNombrePages();
    }


}