package fr.jv.sdbmrest;



import fr.jv.sdbmrest.dto.CouleurDTO;
import fr.jv.sdbmrest.security.Tokened;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Couleur;

import java.util.ArrayList;
import java.util.List;

@Path("couleurs")
@Tag(name="Couleurs", description="couleurs des bières")
@Produces(MediaType.APPLICATION_JSON)
public class CouleursResource {

    @Context
    UriInfo uriInfo;


    @Operation(description = "Affiche toutes les couleurs")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCouleurs() {
        ArrayList<Couleur> listeCouleurs = DAOFactory.getCouleurDAO().getAll();
        List<CouleurDTO> listeCouleursDTO = CouleurDTO.toDTOList(listeCouleurs,
                UriBuilder.fromUri(uriInfo.getBaseUri()).path("couleurs"));
        return Response.status(200).entity(listeCouleursDTO).build();

    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(description="Affiche couleur par id")
    public Response getCouleurByID(@PathParam("id") String id) {
        Couleur couleur = DAOFactory.getCouleurDAO().getById(Integer.parseInt(id));
        if(couleur == null){
            return Response.ok("Ressource introuvable").build();
        }
        CouleurDTO couleurDTO = new CouleurDTO(couleur, UriBuilder.fromUri(uriInfo.getBaseUri()).path("couleurs"));
        return Response.ok(couleurDTO).build();

    }

    @Path("{id}/articles")
    @GET
    @Operation(description = "affiche les articles pour une couleur donnée")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticlesByCouleur(@PathParam("id") String id) {
        return null;

    }

    @Tokened
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCouleur(Couleur couleur){
        if(couleur == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        boolean created = DAOFactory.getCouleurDAO().insert(couleur);
        if(created){
            return Response.ok(couleur).status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();

    }

    @Path("{id}")
    @Tokened
    @DELETE
    public Response deleteCouleur(@PathParam("id") int id){
        Couleur couleur = DAOFactory.getCouleurDAO().getById(id);
        if(couleur == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        boolean deleted = DAOFactory.getCouleurDAO().delete(couleur);
        if(deleted) {
            return Response.ok(couleur).status(Response.Status.NO_CONTENT).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();

    }

    @Path("{id}")
    @Tokened
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCouleur(@PathParam("id") int id, Couleur couleur){
        if(couleur == null || !couleur.getId().equals(id)){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        boolean updated = DAOFactory.getCouleurDAO().update(couleur);
        if(updated) {
            CouleurDTO couleurDTO = new CouleurDTO(couleur, UriBuilder.fromUri(uriInfo.getBaseUri()).path("couleurs"));
            return Response.ok(couleurDTO).status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}