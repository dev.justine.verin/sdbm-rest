package fr.jv.sdbmrest.sdbmclientpc.dao;


public class DAOFactory {

    private DAOFactory(){

    }
    public static ArticleDAO getArticleDAO() {return new ArticleDAO();}
    public static CouleurDAO getCouleurDAO() {
        return new CouleurDAO();
    }
    public static ContinentDAO getContinentDAO() { return new ContinentDAO();}
    public static FabricantDAO getFabricantDAO() {return new FabricantDAO();}
    public static MarqueDAO getMarqueDAO() {return new MarqueDAO();}

    public static PaysDAO getPaysDAO() {return new PaysDAO();}
    public static TicketDAO getTicketDAO(){ return new TicketDAO();}
    public static TypebiereDAO getTypebiereDAO() {return new TypebiereDAO();}
    public static VendreDAO getVendreDAO() {return new VendreDAO();}

    public static UtilisateurDAO getUtilisateurDAO() {return new UtilisateurDAO();}

}