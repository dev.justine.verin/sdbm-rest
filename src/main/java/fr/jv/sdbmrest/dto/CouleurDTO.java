package fr.jv.sdbmrest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.jv.sdbmrest.hateoas.HateOAS;
import fr.jv.sdbmrest.hateoas.Link;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Couleur;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.core.UriBuilder;


import java.util.ArrayList;
import java.util.List;

public class CouleurDTO extends HateOAS {
    @JsonProperty(index = 1)
    private Integer id;
    @JsonProperty(index = 2)
    private String libelle;
    public CouleurDTO(Couleur couleur){
        this.id = couleur.getId();
        this.libelle = couleur.getLibelle();


    }

    public CouleurDTO(Couleur couleur, UriBuilder uriBuilder){
        Couleur newCouleur = new Couleur(0, "libelleCouleur");
        this.id = couleur.getId();
        this.libelle = couleur.getLibelle();
        this.addLink(new Link(HttpMethod.GET, uriBuilder.clone().path(id.toString()).build(), "self"));
        this.addLink(new Link(HttpMethod.GET, uriBuilder.clone().build(), "all"));
        this.addLink(new Link(HttpMethod.PUT, uriBuilder.clone().path(id.toString()).build(), "update", newCouleur));
        this.addLink(new Link(HttpMethod.POST, uriBuilder.clone().build(), "new", newCouleur));
        if(couleur.getNbArticles() == 0){
            this.addLink(new Link(HttpMethod.DELETE, uriBuilder.clone().path(id.toString()).build(), "delete"));

        }


    }



    public static List<CouleurDTO> toDTOList(List<Couleur> listeCouleurs, UriBuilder uriBuilder){
        List<CouleurDTO> listeCouleursDTO = new ArrayList<>();
        for(Couleur couleur : listeCouleurs){
            CouleurDTO couleurDTO = new CouleurDTO(couleur, uriBuilder);
            listeCouleursDTO.add(couleurDTO);
        }
        return listeCouleursDTO;

    }



}