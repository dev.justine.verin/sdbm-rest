package fr.jv.sdbmrest.sdbmclientpc.connections;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import java.sql.Connection;

public class SDBMConnection1 {
    private static Connection connexion;
    private SDBMConnection1(){


    }
    public static Connection getInstance(){
        if(connexion == null){
            try{
                SQLServerDataSource dataSource = new SQLServerDataSource();
                dataSource.setServerName("127.0.0.1");
                dataSource.setPortNumber(1433);
                dataSource.setDatabaseName("SDBM");
                dataSource.setIntegratedSecurity(false);
                dataSource.setEncrypt(false);
                dataSource.setUser("devSDBM");
                dataSource.setPassword("J2ptl@cTM2cvfc");
                connexion = dataSource.getConnection();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }

        return connexion;
    }





}