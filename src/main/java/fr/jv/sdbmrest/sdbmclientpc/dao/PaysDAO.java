package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Pays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PaysDAO extends DAO<Pays, Pays, Integer> {
    @Override
    public Pays getById(Integer id) {
        String sqlStatement = "SELECT ID_PAYS, NOM_PAYS, P.ID_CONTINENT, NOM_CONTINENT" +
                " FROM PAYS AS P " +
                "INNER JOIN CONTINENT as C " +
                "ON C.ID_CONTINENT = P.ID_CONTINENT " +
                "WHERE ID_PAYS =" + id;

        try(Statement statement = connexion.createStatement()){  //exemple try with
            // resources
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            if(resultSet.next())
                return new Pays(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        new Continent(resultSet.getInt(3), resultSet.getString(4)));
            return null;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public ArrayList<Pays> getAll() {
        ArrayList<Pays> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_PAYS, NOM_PAYS, P.ID_CONTINENT, NOM_CONTINENT" +
                " FROM PAYS AS P " +
                "INNER JOIN CONTINENT as C " +
                "ON C.ID_CONTINENT = P.ID_CONTINENT ";
        try (Statement statement = connexion.createStatement()){
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while(resultSet.next()){
                liste.add(new Pays(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        new Continent(resultSet.getInt(3), resultSet.getString(4)
                        )));
            }
            resultSet.close();

        }
        catch(SQLException exception){
            exception.printStackTrace();
        }
        return liste;


    }

    @Override
    public ArrayList<Pays> getLike(Pays object) {
        return new ArrayList<>();
    }

    public ArrayList<Pays> getByContinent(Continent continent){
        ArrayList<Pays> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_PAYS, NOM_PAYS from PAYS WHERE " +
                "ID_CONTINENT = ?";
        try(PreparedStatement preparedStatement = connexion.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, continent.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                liste.add(new Pays(resultSet.getInt("ID_PAYS"),
                        resultSet.getString("NOM_PAYS"), continent ));

            }
        }catch(SQLException exception){
            exception.printStackTrace();
        }
        return liste;

    }

    @Override
    public boolean insert(Pays object) {
        String sqlStatement = "INSERT INTO PAYS (NOM_PAYS, ID_CONTINENT) VALUES (?, ?)";
        try(PreparedStatement preparedStatement = connexion.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1, object.getLibelle());
            preparedStatement.setInt(2, object.getContinent().getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                object.setId(resultSet.getInt(1));
                return true;
            }
            return false;

        }catch(SQLException exception){
            exception.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean update(Pays object) {
        String sqlStatement = "UPDATE PAYS SET NOM_PAYS = ? WHERE ID_PAYS = ?";
        try(PreparedStatement preparedStatement = connexion.prepareStatement(sqlStatement)){
            preparedStatement.setString(1, object.getLibelle());
            preparedStatement.setInt(2, object.getId());
            preparedStatement.executeUpdate();
            return true;

        }catch(SQLException exception){
            exception.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean delete(Pays object) {
        String sqlStatement = "DELETE FROM PAYS WHERE ID_PAYS = ?";
        try(PreparedStatement preparedStatement = connexion.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, object.getId());
            preparedStatement.executeUpdate();
            return true;
        }catch(SQLException exception){
            exception.printStackTrace();
            return false;
        }
    }
}