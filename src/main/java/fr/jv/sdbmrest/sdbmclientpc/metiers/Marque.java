package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Marque {
    private Integer id;
    private String libelle;
    private Fabricant fabricant;
    private Pays pays;

    @Override
    public String toString(){
        return libelle;

    }

    @Override
    public boolean equals(Object other){
        if(other == this )
            return true;
        if(!(other instanceof Marque ))
            return false;
        Marque marque = (Marque) other;
        return marque.id == this.id;

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}