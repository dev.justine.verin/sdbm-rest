package fr.jv.sdbmrest;


import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Pays;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static io.restassured.RestAssured.given;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ContinentResourceTest {
    static int idPays;
    static int idContinent;


    @Order(1)
    void dummyRequest(){
        given().get("api/couleurs/1");
    }
    @Test
    @Order(2)
    void getContinents() {
        idContinent = 1;
        Continent[] tableau = given()
                .get("api/continents")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(200)
                .extract().as(Continent[].class);

        List<Continent> continents = Arrays.stream(tableau).toList();
        assert continents.contains(DAOFactory.getContinentDAO().getById(idContinent));


    }

    @Test
    @Order(3)
    void getContinent() {
        idContinent = 1;
        Continent continent =
                given()
                        .get("api/continents/" + idContinent)
                        .then().contentType(MediaType.APPLICATION_JSON)
                        .statusCode(200)
                        .extract().as(Continent.class);
        assert continent.getId() == idContinent;


    }

    @Test
    @Order(4)
    void getPaysByContinent() {
        idPays = 1;
        Pays[] tableau =
                given()
                        .get("api/continents/" + idContinent + "/pays")
                        .then().contentType(MediaType.APPLICATION_JSON)
                        .statusCode(200)
                        .extract().as(Pays[].class);
        List<Pays> pays = Arrays.stream(tableau).toList();
        assert pays.contains(DAOFactory.getPaysDAO().getById(idPays));



    }

    @Test
    @Order(5)
    void addPaysToContinent() {
        idContinent = 4;
        Continent continent = DAOFactory.getContinentDAO().getById(idContinent);
        Pays paysToAdd = new Pays(0, "Azerbaïdjan", continent );
        Pays paysAdded = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(paysToAdd)
                .post("api/continents/" + idPays + "/pays")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(201)
                .extract().as(Pays.class);
        idPays = paysAdded.getId();
        assert !Objects.equals(paysAdded.getId(), paysToAdd.getId());

    }

    @Test
    @Order(6)
    void updatePaysInContinent() {
        Pays pays = DAOFactory.getPaysDAO().getById(idPays);
        pays.setLibelle("Ouzbékistan");
        Pays paysModifie = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(pays)
                .put("api/continents/" + idContinent + "/pays")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().as(Pays.class);
        assert pays.getLibelle().equals(paysModifie.getLibelle());
    }



    @Test
    @Order(7)
    void deletePaysFromContinent() {
        Pays pays = DAOFactory.getPaysDAO().getById(idPays);
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(pays)
                .delete("api/continents/" + idContinent + "/pays/")
                .then()
                .statusCode(HttpServletResponse.SC_NO_CONTENT);
        assert DAOFactory.getPaysDAO().getById(idPays) == null;


    }

}