package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Typebiere;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TypebiereDAO extends DAO<Typebiere, Typebiere, Integer> {
    @Override
    public Typebiere getById(Integer id) {
        String sqlRequest = "SELECT ID_TYPE, NOM_TYPE FROM TYPEBIERE where " +
                "ID_TYPE = " + id;

        try(Statement statement = connexion.createStatement()){  //exemple try with
            // resources
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if(resultSet.next())
                return new Typebiere(resultSet.getInt(1), resultSet.getString(2));
            return null;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Typebiere> getAll() {
        ArrayList<Typebiere> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_TYPE, NOM_TYPE from TYPEBIERE";
        try (Statement statement = connexion.createStatement()){
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while(resultSet.next()){
                liste.add(new Typebiere(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();

        }
        catch(SQLException exception){
            exception.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Typebiere> getLike(Typebiere object) {
        return null;
    }

    @Override
    public boolean insert(Typebiere object) {
        return false;
    }

    @Override
    public boolean update(Typebiere object) {
        return false;
    }

    @Override
    public boolean delete(Typebiere object) {
        return false;
    }
}