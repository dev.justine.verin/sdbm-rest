package fr.jv.sdbmrest.security;

import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

public class Argon2 {
    private static final Argon2PasswordEncoder encoder = new Argon2PasswordEncoder(16, 100, 1, 1024, 20);
    public static final String PREFIX = "$argon2id$v=19$m=1024,t=20,p=1$";
    public static String encode(String password) {
        String hashedPassword = encoder.encode(password);
        String hashedAndCutPassword = hashedPassword.substring(PREFIX.length(), hashedPassword.length());
        return hashedAndCutPassword;

    }

    public static boolean verify(String rawPassword, String hashedAndCutPassword){
        return encoder.matches(rawPassword, PREFIX + hashedAndCutPassword);
    }


}