package fr.jv.sdbmrest.sdbmclientpc.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SDBMConnection2 {
    private static Connection connexion;
    private static String url = "jdbc:sqlserver://localhost:1433;databaseName=SDBM;" +
            "encrypt=false";
    private static String className = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static String utilisateur = "devSDBM";
    private static String motDePasse = "J2ptl@cTM2cvfc";


    private SDBMConnection2(){


    }

    public static Connection getInstance(){
        if(connexion == null){
            try{
                Class.forName(className);
                connexion = DriverManager.getConnection(url, utilisateur, motDePasse);

            }
            catch(ClassNotFoundException classExp){
                System.out.println("Driver non trouvé");
            }
            catch(SQLException sqlExp){
                sqlExp.printStackTrace();
            }
        }
        return connexion;
    }





}