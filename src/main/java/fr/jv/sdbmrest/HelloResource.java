package fr.jv.sdbmrest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
@Tag(name="Hello world", description= "Hello world")
@Path("/hello-world")
public class HelloResource {
    @GET
    @Produces("text/plain")
    @Operation(description = "GET Hello World")
    public String hello() {
        return "Hello, World!";
    }
}