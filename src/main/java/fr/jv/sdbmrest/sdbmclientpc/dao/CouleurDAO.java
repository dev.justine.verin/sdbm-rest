package fr.jv.sdbmrest.sdbmclientpc.dao;

import fr.jv.sdbmrest.sdbmclientpc.metiers.Couleur;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CouleurDAO extends DAO<Couleur, Couleur, Integer> {

    @Override
    public Couleur getById(Integer id) {
        String sqlRequest = "SELECT ID_COULEUR, NOM_COULEUR FROM COULEUR where " +
                "ID_COULEUR = " + id;

        try (Statement statement = connexion.createStatement()) {  //exemple try with
            // resources
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if (resultSet.next())
                return new Couleur(resultSet.getInt(1), resultSet.getString(2));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Couleur> getAll() {
        ArrayList<Couleur> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_COULEUR, NOM_COULEUR from COULEUR";
        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while (resultSet.next()) {
                liste.add(new Couleur(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Couleur> getLike(Couleur object) {
        return null;
    }

    @Override
    public boolean insert(Couleur object) {
        String sqlStatement = "INSERT INTO COULEUR VALUES (?)";
        try (PreparedStatement statement = connexion.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, object.getLibelle());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                object.setId(resultSet.getInt(1));
                return true;
            }
            return false;
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean update(Couleur object) {
        String sqlStatement = "UPDATE COULEUR SET NOM_COULEUR = ?  WHERE ID_COULEUR = ?";
        try (PreparedStatement statement = connexion.prepareStatement(sqlStatement)) {
            statement.setString(1, object.getLibelle());
            statement.setInt(2, object.getId());
            statement.executeUpdate();
            object.setLibelle(this.getById(object.getId()).getLibelle());
            return true;

        }catch(SQLException exception){
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Couleur object) {
        String sqlStatement = "DELETE FROM COULEUR WHERE ID_COULEUR = ?";
        try (PreparedStatement statement = connexion.prepareStatement(sqlStatement)) {
            statement.setInt(1, object.getId());
            statement.executeUpdate();
            return true;

        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    public int getNombreArticles(Couleur object) {
        int nbreArticles = 0;
        String sqlStatement = "SELECT COUNT(*) FROM ARTICLE WHERE ID_COULEUR = ?";
        try (PreparedStatement statement = connexion.prepareStatement(sqlStatement)) {
            statement.setInt(1, object.getId());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                nbreArticles = resultSet.getInt(1);
        }
        catch(SQLException exception){
            exception.printStackTrace();
            nbreArticles = 1;
        }
        return nbreArticles;

    }


}