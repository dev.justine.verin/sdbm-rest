package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Vendre;
import fr.jv.sdbmrest.sdbmclientpc.metiers.VendreID;

import java.util.ArrayList;

public class VendreDAO extends DAO<Vendre, Vendre, VendreID>{


    @Override
    public Vendre getById(VendreID object) {
        return null;
    }

    @Override
    public ArrayList<Vendre> getAll() {
        return null;
    }

    @Override
    public ArrayList<Vendre> getLike(Vendre object) {
        return null;
    }

    @Override
    public boolean insert(Vendre object) {
        return false;
    }

    @Override
    public boolean update(Vendre object) {
        return false;
    }

    @Override
    public boolean delete(Vendre object) {
        return false;
    }
}