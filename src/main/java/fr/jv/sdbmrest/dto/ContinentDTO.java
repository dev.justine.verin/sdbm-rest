package fr.jv.sdbmrest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.jv.sdbmrest.hateoas.HateOAS;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;

import java.util.ArrayList;

public class ContinentDTO extends HateOAS {
    @JsonProperty(index = 1)
    private Integer id;
    @JsonProperty(index = 2)
    private String libelle;
    @JsonProperty(index = 3)
    private ArrayList<PaysDTO> pays;


    public ContinentDTO(Continent continent){


    }



}