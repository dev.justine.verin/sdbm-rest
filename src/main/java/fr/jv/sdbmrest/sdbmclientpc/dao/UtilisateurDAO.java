package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Utilisateur;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UtilisateurDAO extends DAO<Utilisateur, Utilisateur,String> {

    @Override
    public Utilisateur getById(String login) {
        String sqlStatement = "SELECT login, passwordHashed, email, role from Utilisateur where login =?";
        try(PreparedStatement ps = connexion.prepareStatement(sqlStatement)){
            ps.setString(1, login);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()){
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setLogin(resultSet.getString("login"));
                utilisateur.setPassword(resultSet.getString("passwordHashed"));
                utilisateur.setEmail(resultSet.getString("email"));
                utilisateur.setRole(resultSet.getString("role"));
                return utilisateur;
            }
        }
        catch(SQLException exception){
            exception.printStackTrace();
        }
        return null;
    }

    public Utilisateur getByEmail(String email){
        String sqlStatement = "SELECT login from Utilisateur where email = ?";
        try(PreparedStatement ps = connexion.prepareStatement(sqlStatement)) {
            ps.setString(1, email);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setLogin(resultSet.getString("login"));
                return utilisateur;
            }
        }
        catch(SQLException exception){
            exception.printStackTrace();

        }
        return null;
    }

    @Override
    public ArrayList<Utilisateur> getAll() {
        return null;
    }

    @Override
    public ArrayList<Utilisateur> getLike(Utilisateur utilisateur) {
        return null;
    }

    @Override
    public boolean insert(Utilisateur utilisateur) {
        return false;
    }

    @Override
    public boolean update(Utilisateur utilisateur) {
        return false;
    }

    @Override
    public boolean delete(Utilisateur utilisateur) {
        return false;
    }
}