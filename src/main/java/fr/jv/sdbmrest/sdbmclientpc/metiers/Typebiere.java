package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Typebiere {
    private Integer id;
    private String libelle;
    @Override
    public String toString(){
        return libelle;

    }
    
    @Override
    public boolean equals(Object other){
        if(other == this )
            return true;
        if(!(other instanceof Typebiere ))
            return false;
        Typebiere typebiere = (Typebiere) other;
        return typebiere.id == this.id;

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}