package fr.jv.sdbmrest.hateoas;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.net.URI;
@Getter
@Setter
public class Link {
    private String method;
    private URI uri;
    private String name;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private Object parametre;


    public Link(String method, URI uri, String name) {
        this.method = method;
        this.uri = uri;
        this.name = name;
    }

    public Link(String method, URI uri, String name, Object parametre) {
        this.method = method;
        this.uri = uri;
        this.name = name;
        this.parametre = parametre;
    }
}