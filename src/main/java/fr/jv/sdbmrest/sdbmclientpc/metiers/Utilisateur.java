package fr.jv.sdbmrest.sdbmclientpc.metiers;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.security.Argon2;

@Getter
@Setter
@NoArgsConstructor
public class Utilisateur {
    private String login;
    private String password;
    private String email;
    private String role;
    private Boolean valide;

    public Utilisateur(String login, String password){
        valide = false;
        Utilisateur userFromDB = DAOFactory.getUtilisateurDAO().getById(login);
        if(userFromDB!= null && Argon2.verify(password, userFromDB.getPassword())){
            this.login = login;
            this.password = userFromDB.getPassword();
            this.email = userFromDB.getEmail();
            this.role = userFromDB.getRole();
            this.valide = true;
        }


    }




}