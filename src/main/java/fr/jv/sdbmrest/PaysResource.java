package fr.jv.sdbmrest;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.GenericEntity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Pays;

import java.util.List;

@Path("pays")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name= "pays")
public class PaysResource {
    @GET
    @Operation(description = "Recupère tous les pays")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"), @ApiResponse(responseCode = "404",
    description = "ressource non trouvée")})
    public Response getAllPays(){
        List<Pays> pays = DAOFactory.getPaysDAO().getAll();
        return Response.ok(new GenericEntity<>(pays) {
        }).build();
    }


}