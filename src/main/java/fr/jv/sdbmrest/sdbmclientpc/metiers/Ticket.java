package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.sql.Time;


@AllArgsConstructor
@Getter
@Setter
public class Ticket {
    private TicketID ticketID;
    private Date dateVente;
    private Time heureVente;


}