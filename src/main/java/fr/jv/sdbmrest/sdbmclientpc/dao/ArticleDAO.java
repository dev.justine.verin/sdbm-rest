package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.*;
import fr.jv.sdbmrest.sdbmclientpc.services.ArticleSearch;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class ArticleDAO extends DAO<Article, ArticleSearch, Integer> {
    private static final String ID_ARTICLE = "ID_ARTICLE";
    private static final String NOM_ARTICLE = "NOM_ARTICLE";
    private static final String ID_COULEUR = "ID_COULEUR";
    private static final String NOM_COULEUR = "NOM_COULEUR";
    private static final String ID_TYPE = "ID_TYPE_BIERE";
    private static final String NOM_TYPE = "NOM_TYPE";


    private static final String ID_MARQUE = "ID_MARQUE";
    private static final String NOM_MARQUE = "NOM_MARQUE";
    private static final String ID_FABRICANT = "ID_FABRICANT";
    private static final String NOM_FABRICANT = "NOM_FABRICANT";
    private static final String ID_PAYS = "ID_PAYS";
    private static final String NOM_PAYS = "NOM_PAYS";
    private static final String ID_CONTINENT = "ID_CONTINENT";
    private static final String NOM_CONTINENT = "NOM_CONTINENT";

    private static final String VOLUME = "VOLUME";
    private static final String PRIX_ACHAT = "PRIX_ACHAT";
    private static final String TITRAGE = "TITRAGE";


    public ArticleDAO(){
       Logger.getLogger(getClass().getName());
    }



    @Override
    public Article getById(Integer id) {
        String sqlStatement = "SELECT ID_ARTICLE, NOM_ARTICLE, ID_COULEUR, " +
                "NOM_COULEUR," +
                " ID_TYPE_BIERE, NOM_TYPE, ID_MARQUE, NOM_MARQUE, ID_PAYS, NOM_PAYS, " +
                "ID_CONTINENT, NOM_CONTINENT, VOLUME, TITRAGE, PRIX_ACHAT " +
                "from Vue_Article " +
                " WHERE ID_ARTICLE = " + id;

        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            if (resultSet.next()) {
                return new Article(
                        resultSet.getInt(ID_ARTICLE),
                        resultSet.getString(NOM_ARTICLE),
                        new Couleur(resultSet.getInt(ID_COULEUR),
                                resultSet.getString(NOM_COULEUR)),
                        new Typebiere(resultSet.getInt(ID_TYPE), resultSet.getString(
                                NOM_TYPE)),
                        new Marque(resultSet.getInt(ID_MARQUE), resultSet.getString(
                                NOM_MARQUE),
                                new Fabricant(resultSet.getInt(ID_FABRICANT),
                                        resultSet.getString(NOM_FABRICANT)),
                                new Pays(resultSet.getInt(ID_PAYS),
                                        resultSet.getString(NOM_PAYS),
                                        new Continent(resultSet.getInt(ID_CONTINENT),
                                                resultSet.getString(NOM_CONTINENT)))),
                        resultSet.getInt(VOLUME),
                        resultSet.getBigDecimal(TITRAGE),
                        resultSet.getBigDecimal(PRIX_ACHAT)

                );

            }
            return null;


        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }


    }

    @Override
    public ArrayList<Article> getAll() {
        ArrayList<Article> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_ARTICLE, NOM_ARTICLE, A.ID_COULEUR, " +
                "NOM_COULEUR," +
                " A.ID_TYPE_BIERE, NOM_TYPE, A.ID_MARQUE, NOM_MARQUE, M.ID_PAYS, NOM_PAYS, " +
                "P.ID_CONTINENT, NOM_CONTINENT, VOLUME, TITRAGE, PRIX_ACHAT " +
                "from ARTICLE as A " +
                "INNER JOIN COULEUR as C on C.ID_COULEUR = A.ID_COULEUR " +
                "INNER JOIN TYPEBIERE as TB on TB.ID_TYPE = A.ID_TYPE_BIERE " +
                "INNER JOIN MARQUE as M on M.ID_MARQUE = A.ID_MARQUE " +
                "INNER JOIN FABRICANT AS F on M.ID_FABRICANT = F.ID_FABRICANT " +
                "INNER JOIN PAYS as P on M.ID_PAYS = P.ID_PAYS " +
                "INNER JOIN CONTINENT as C on C.ID_CONTINENT = P.ID_CONTINENT ";
        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while (resultSet.next()) {
                liste.add(new Article(
                        resultSet.getInt(ID_ARTICLE),
                        resultSet.getString(NOM_ARTICLE),
                        new Couleur(resultSet.getInt(ID_COULEUR),
                                resultSet.getString(NOM_COULEUR)),
                        new Typebiere(resultSet.getInt(ID_TYPE), resultSet.getString(
                                NOM_TYPE)),
                        new Marque(resultSet.getInt(ID_MARQUE), resultSet.getString(
                                NOM_MARQUE),
                                new Fabricant(resultSet.getInt(ID_FABRICANT),
                                        resultSet.getString(NOM_FABRICANT)),
                                new Pays(resultSet.getInt(ID_PAYS),
                                        resultSet.getString(NOM_PAYS),
                                        new Continent(resultSet.getInt(ID_CONTINENT),
                                                resultSet.getString(NOM_CONTINENT)))),
                        resultSet.getInt(VOLUME),
                        resultSet.getBigDecimal(TITRAGE),
                        resultSet.getBigDecimal(PRIX_ACHAT)

                ));
            }
            resultSet.close();

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return liste;

    }

    @Override
    public ArrayList<Article> getLike(ArticleSearch articleSearch) {
        ArrayList<Article> listeArticles = new ArrayList<>();
        String sqlStoredProcedureCall = "{call ps_qbe_vue_article(?, ?, ?, ?, ?, ? ,?," +
                " " +
                "?, ?, ?, ?, ?, ?" +
                " )}";
        try (CallableStatement call = connexion.prepareCall(sqlStoredProcedureCall)) {
            setParameters(call, articleSearch);
            ResultSet resultSet = call.executeQuery();

            while (resultSet.next()) {
                Article articleTrouve = new Article(resultSet.getInt(ID_ARTICLE),
                        resultSet.getString(NOM_ARTICLE),
                        resultSet.getInt(VOLUME), resultSet.getBigDecimal(TITRAGE),
                        resultSet.getBigDecimal(PRIX_ACHAT));
                articleTrouve.setCouleur(new Couleur(resultSet.getInt(ID_COULEUR), resultSet.getString(NOM_COULEUR)));
                articleTrouve.setTypebiere(new Typebiere(resultSet.getInt(ID_TYPE), resultSet.getString(NOM_TYPE)));
                articleTrouve.setMarque(new Marque(resultSet.getInt(ID_MARQUE), resultSet.getString(NOM_MARQUE), null,
                        null));
                articleTrouve.getMarque().setFabricant(new Fabricant(resultSet.getInt(ID_FABRICANT),
                        resultSet.getString(NOM_FABRICANT)));
                articleTrouve.getMarque().setPays(new Pays(resultSet.getInt(ID_PAYS), resultSet.getString(NOM_PAYS),
                        null));
                articleTrouve.getMarque().getPays().setContinent(new Continent(resultSet.getInt(ID_CONTINENT),
                        resultSet.getString(NOM_CONTINENT)));
                listeArticles.add(articleTrouve);

            }
            articleSearch.setNombreLignesTotal(call.getInt(13));


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeArticles;
    }

    private void setParameters(CallableStatement call, ArticleSearch articleSearch) {
        try {
            call.setString("nom_article", articleSearch.getLibelleArticle());
            call.setInt("volume", articleSearch.getVolume());
            call.setFloat("titrage_min",
                    articleSearch.getTitrageMin());
            call.setFloat("titrage_max", articleSearch.getTitrageMax());
            call.setInt("id_couleur",
                    articleSearch.getCouleur().getId());
            call.setInt("id_type",
                    articleSearch.getTypebiere().getId());
            call.setInt("id_marque", articleSearch.getMarque().getId());
            call.setInt("id_fabricant", articleSearch.getFabricant().getId());
            call.setInt("id_pays",
                    articleSearch.getPays().getId());
            call.setInt("id_continent", articleSearch.getContinent().getId());
            call.setInt("numero_page", articleSearch.getNumeroPage());
            call.setInt("lignes_par_page", articleSearch.getLignesParPage());
            call.registerOutParameter(13, Types.INTEGER);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }


    }

    @Override
    public boolean insert(Article object) {
        return false;
    }

    @Override
    public boolean update(Article article) {
        String sqlStatement = "update ARTICLE set NOM_ARTICLE = ?, ID_COULEUR = ?, ID_TYPE_BIERE = ?, ID_MARQUE = ?, " +
                "TITRAGE" +
                " " +
                "= ? , PRIX_ACHAT =?, VOLUME =? where ID_ARTICLE = ?" ;
        try (PreparedStatement statement = connexion.prepareStatement(sqlStatement)) {
            statement.setString(1, article.getLibelle());
            statement.setInt(2, article.getCouleur().getId());
            statement.setInt(3, article.getTypebiere().getId());
            statement.setInt(4, article.getMarque().getId());
            statement.setBigDecimal(5, article.getTitrage());
            statement.setBigDecimal(6, article.getPrixAchat());
            statement.setInt(7, article.getVolume());
            statement.setInt(8, article.getId());
            statement.executeUpdate();
            return true;
        }
        catch(SQLException sqlException){
            return false;
        }



    }

    @Override
    public boolean delete(Article object) {
        return false;
    }





}