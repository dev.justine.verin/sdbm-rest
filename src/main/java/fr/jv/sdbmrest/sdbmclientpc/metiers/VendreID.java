package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class VendreID {

    private TicketID ticketID;
    private Article article;

}