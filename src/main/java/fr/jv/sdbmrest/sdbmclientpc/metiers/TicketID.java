package fr.jv.sdbmrest.sdbmclientpc.metiers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor


public class TicketID {
    private Integer annee;
    private Integer numeroTicket;


}