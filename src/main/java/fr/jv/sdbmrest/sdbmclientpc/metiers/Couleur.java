package fr.jv.sdbmrest.sdbmclientpc.metiers;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
public class Couleur {
        @Schema(name="id", description = "id couleur", example = "999")
        private Integer id;
        @Schema(name="libelle", description = "nom de la couleur", example = "Bleu canard")
        private String libelle;

        @JsonIgnore
        private int nbArticles;
        public Couleur(Integer id, String libelle) {
                this.id = id;
                this.libelle = libelle;
                this.nbArticles = id != null ? DAOFactory.getCouleurDAO().getNombreArticles(this) : 0;


        }

        public Couleur(){

        }
        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getLibelle() {
                return libelle;
        }

        public void setLibelle(String libelle) {
                this.libelle = libelle;
        }

        @Override
        public String toString(){
                return libelle;
        }

        @Override
        public boolean equals(Object other){
                if(other == this )
                        return true;
                if(!(other instanceof Couleur ))
                        return false;
                Couleur couleur = (Couleur) other;
                return couleur.id == this.id;


        }

        @Override
        public int hashCode() {
                return super.hashCode();
        }
}