package fr.jv.sdbmrest.sdbmclientpc.metiers;

public class Volume {

    private Integer valeurVolume;

    private String libelleVolume;


    public Volume(int valeurVolume){
        this.valeurVolume = valeurVolume;
        libelleVolume = String.valueOf(this.valeurVolume);

    }

    public Integer getValeurVolume() {
        return valeurVolume;
    }

    public String getLibelleVolume() {
        return libelleVolume;
    }

    public void setLibelleVolume(String libelleVolume){
        this.libelleVolume = libelleVolume;
    }

    @Override
    public String toString(){
        return libelleVolume;
    }
}