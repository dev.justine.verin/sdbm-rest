package fr.jv.sdbmrest;

import fr.jv.sdbmrest.sdbmclientpc.metiers.Utilisateur;
import fr.jv.sdbmrest.security.MyToken;
import fr.jv.sdbmrest.security.User;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthentificationResourceTest {
    @Order(1)
    void dummyRequest() {
        given().get("api");

    }

    @Order(2)
    @Test
    void loginOK(){
        User user = new User();
        user.setLogin("user");
        user.setPassword("userpassword");
        String token =
                given().contentType(MediaType.APPLICATION_JSON).body(user).post("api/login").then().statusCode(200).extract().asString();
        assert(MyToken.getRole(token).equals("user"));

    }

    @Order(3)
    @Test
    void loginNotOK(){
        User user = new User();
        user.setLogin("wrong");
        user.setPassword("wrongpassword");
        given().contentType(MediaType.APPLICATION_JSON).body(user).post("api/login").then().statusCode(401);
    }
}