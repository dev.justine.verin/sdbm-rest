package fr.jv.sdbmrest;


import fr.jv.sdbmrest.security.Tokened;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.GenericEntity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Pays;

import java.util.List;

@Path("continents")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "continents", description = "continents et pays qui leur sont associés")
public class ContinentResource {

    @GET
    @Operation( description = "Récupère tous les continents avec liste des pays associés")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"),
             @ApiResponse(responseCode = "404",
            description = "ressource non trouvée")})
    public Response getContinents() {
        List<Continent> continents = DAOFactory.getContinentDAO().getAll();
        return Response.ok(new GenericEntity<>(continents) {
        }).build();
    }

    @GET
    @Path("{id}")
    @Operation(description = "Récupère un seul continent identifié par son id")
    @ApiResponses(value = { @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404",
            description = "ressource non trouvée")})
    public Response getContinent(@PathParam("id") int id) {
        Continent continent = DAOFactory.getContinentDAO().getById(id);
        return Response.ok(continent).build();
    }



    @GET
    @Path("{id}/pays")
    @Operation(description = "Récupère les pays associés à un seul continent")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "404",
            description = "ressource non trouvée")})
    public Response getPaysByContinent(@PathParam("id") int id) {
        List<Pays> pays = DAOFactory.getPaysDAO().getByContinent(new Continent(id, ""));
        return Response.ok(pays).build();
    }

    @POST
    @Tokened
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/pays")
    @Operation(description="Ajoute un pays au continent de l'id passé en paramètre de la requête")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "pays ajouté"),
            @ApiResponse(responseCode = "400", description = "échec création pays")})
    public Response addPaysToContinent(@PathParam("id") int id, Pays pays) {
        if (pays == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Continent continent = DAOFactory.getContinentDAO().getById(id);
        pays.setContinent(continent);
        boolean created = DAOFactory.getPaysDAO().insert(pays);
        if (created) {
            return Response.ok(pays).status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();


    }

    @PUT
    @Tokened
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/pays")
    @Operation(description="Modifie un pays dans le continent de l'id passé en paramètre de la " +
            "requête")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "pays modifié"),
            @ApiResponse(responseCode = "400", description = "échec modification pays")})
    public Response updatePaysInContinent(@PathParam("id") int id, Pays pays) {

        if (pays == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        boolean updated = DAOFactory.getPaysDAO().update(pays);
        if (updated) {
            return Response.ok(pays).status(Response.Status.CREATED).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();

    }


    @DELETE
    @Tokened
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}/pays")
    @Operation(description="Supprime un pays dans le continent de l'id passé en paramètre de la " +
            "requête")
    @ApiResponses(value = {@ApiResponse(responseCode = "202", description = "pays supprimé"),
            @ApiResponse(responseCode = "400", description = "échec suppression pays")})
    public Response deletePaysFromContinent(@PathParam("id") int id, Pays pays) {
        if (pays == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Continent continent = DAOFactory.getContinentDAO().getById(id);
        pays.setContinent(continent);
        boolean deleted = DAOFactory.getPaysDAO().delete(pays);
        if (deleted) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }


}