package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ContinentDAO extends DAO<Continent, Continent, Integer> {
    @Override
    public Continent getById(Integer id) {
        String sqlRequest = "SELECT ID_CONTINENT, NOM_CONTINENT FROM CONTINENT where " +
                "ID_CONTINENT = " + id;

        try(Statement statement = connexion.createStatement()){  
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if(resultSet.next()) {
                Continent continent = new Continent(resultSet.getInt(1), resultSet.getString(2));
                continent.setPays(DAOFactory.getPaysDAO().getByContinent(continent));
                return continent;
            }
            return null;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Continent> getAll() {
        ArrayList<Continent> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_CONTINENT, NOM_CONTINENT from CONTINENT";
        try (Statement statement = connexion.createStatement()){
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while(resultSet.next()){
                Continent continent = new Continent(resultSet.getInt(1), resultSet.getString(2));
                continent.setPays(DAOFactory.getPaysDAO().getByContinent(continent));
                liste.add(continent);
            }
            resultSet.close();

        }
        catch(SQLException exception){
            exception.printStackTrace();
        }
        return liste;
        
       
    }

    @Override
    public ArrayList<Continent> getLike(Continent object) {
        return null;
    }

    @Override
    public boolean insert(Continent object) {
        return false;
    }

    @Override
    public boolean update(Continent object) {
        return false;
    }

    @Override
    public boolean delete(Continent object) {
        return false;
    }
}