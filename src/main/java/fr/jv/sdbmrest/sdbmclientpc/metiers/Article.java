package fr.jv.sdbmrest.sdbmclientpc.metiers;


import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class Article {
    @NonNull
    private Integer id;
    @NonNull
    private String libelle;
    private Couleur couleur;
    private Typebiere typebiere;
    private Marque marque;
    @NonNull
    private Integer volume;
    @NonNull
    private BigDecimal titrage;
    @NonNull
    private BigDecimal prixAchat;

    public Article(Integer integer, String s) {
        this.id = integer;
        this.libelle = s;

    }


    public String toString() {
        return libelle;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this)
            return true;
        if (!(other instanceof Article))
            return false;
        Article article = (Article) other;
        return article.id.equals(this.id);

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}