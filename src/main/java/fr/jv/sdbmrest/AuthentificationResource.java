package fr.jv.sdbmrest;


import fr.jv.sdbmrest.hateoas.HateOAS;
import fr.jv.sdbmrest.hateoas.Link;
import fr.jv.sdbmrest.sdbmclientpc.dao.DAOFactory;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Utilisateur;
import fr.jv.sdbmrest.security.MyToken;
import fr.jv.sdbmrest.security.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;

import java.net.URI;

@Path("login")
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "login", description = "test authentification")

public class AuthentificationResource {

    @Context
    UriInfo uriInfo;
    @POST
    @Operation(description = "récupération du token")
    public Response login(User user){
        Utilisateur utilisateur = new Utilisateur(user.getLogin(), user.getPassword());
        if(utilisateur.getValide()){
            String token = MyToken.generate(user);
            return Response.ok().header(HttpHeaders.AUTHORIZATION, token).build();
        }
        UriBuilder uriBuilder = UriBuilder.fromUri(uriInfo.getRequestUri());
        URI forgottenPasswordURI = uriBuilder.clone().path(user.getLogin()).path("forgottenPassword").build();
        URI forgottenLoginURI = uriBuilder.clone().path("forgottenLogin").queryParam("mail", "xxx").build();
        HateOAS hateOAS = new HateOAS();
        hateOAS.addLink(new Link("GET", forgottenLoginURI, "forgotten login"));
        hateOAS.addLink(new Link("GET", forgottenPasswordURI, "forgotten password"));
        return Response.status(Response.Status.FORBIDDEN).type(MediaType.APPLICATION_JSON).entity(hateOAS).build();


    }


    @GET
    @Operation(description = "mot de passe oublié")
    @Path("{login}/forgottenPassword")
    @Produces(MediaType.TEXT_PLAIN)
    public Response forgottenPassword(@PathParam("login") String login){
        Utilisateur utilisateur = DAOFactory.getUtilisateurDAO().getById(login);
        if(utilisateur == null){
            return Response.status(Response.Status.NOT_FOUND).type(MediaType.TEXT_PLAIN_TYPE).entity("Le nom d'utilisateur n'existe pas!").build();
        }
        String email = utilisateur.getEmail();
        String message =
                "Un email contenant le lien de réinitialisation du mot de passe vous a été envoyé à l'adresse " +
                        "suivante :" + email + ". ";
        return Response.ok(message).build();

    }

    @GET
    @Operation(description = "login oublié")
    @Path("forgottenLogin")
    @Produces(MediaType.TEXT_PLAIN)
    public Response forgottenLogin(@QueryParam("mail") String mail){
        Utilisateur utilisateur = DAOFactory.getUtilisateurDAO().getByEmail(mail);
        if(utilisateur == null){
            return Response.status(Response.Status.NOT_FOUND).entity("L'adresse mail n'existe pas!").build();
        }
        String message = "un email contenant votre nom d'utilisateur a été envoyé à l'adresse mail indiquée.";
        return Response.ok(message).build();

    }




}