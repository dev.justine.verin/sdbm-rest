package fr.jv.sdbmrest.sdbmclientpc.dao;



import fr.jv.sdbmrest.sdbmclientpc.connections.SDBMConnection1;

import java.sql.Connection;
import java.util.ArrayList;

public abstract class DAO <T, TSearch, TID> {

    protected Connection connexion;


    protected DAO(){
        connexion = SDBMConnection1.getInstance();

    }
    public abstract T getById(TID object);
    public abstract ArrayList<T> getAll();

    public abstract ArrayList<T>getLike(TSearch object);

    public abstract boolean insert(T object);
    public abstract boolean update(T object);
    public abstract boolean delete(T object);





}