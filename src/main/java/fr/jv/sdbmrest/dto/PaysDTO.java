package fr.jv.sdbmrest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class PaysDTO {
    @JsonProperty(index = 1)
    private int id;
    @JsonProperty(index = 2)
    private String libelle;



}