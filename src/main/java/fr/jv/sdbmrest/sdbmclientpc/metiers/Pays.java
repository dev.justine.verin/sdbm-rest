package fr.jv.sdbmrest.sdbmclientpc.metiers;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pays {
    @Schema(name="id", example = "17")
    private Integer id;
    @Schema(name="libelle", example="Lichtenstein")
    private String libelle;

    @JsonIgnore
    private Continent continent;

    @Override
    public String toString(){
        return libelle;

    }
    public Pays(String libelle){
        this.libelle = libelle;
    }


    public boolean equals(Object other){
        if(other == this )
            return true;
        if(!(other instanceof Pays ))
            return false;
        Pays pays = (Pays) other;
        return pays.id.equals(this.id);

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}