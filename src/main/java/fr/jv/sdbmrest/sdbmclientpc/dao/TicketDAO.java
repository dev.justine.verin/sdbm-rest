package fr.jv.sdbmrest.sdbmclientpc.dao;



import fr.jv.sdbmrest.sdbmclientpc.metiers.Ticket;
import fr.jv.sdbmrest.sdbmclientpc.metiers.TicketID;

import java.util.ArrayList;

public class TicketDAO extends DAO<Ticket, Ticket, TicketID>{


    @Override
    public Ticket getById(TicketID object) {
        return null;
    }

    @Override
    public ArrayList<Ticket> getAll() {
        return null;
    }

    @Override
    public ArrayList<Ticket> getLike(Ticket object) {
        return null;
    }

    @Override
    public boolean insert(Ticket object) {
        return false;
    }

    @Override
    public boolean update(Ticket object) {
        return false;
    }

    @Override
    public boolean delete(Ticket object) {
        return false;
    }
}