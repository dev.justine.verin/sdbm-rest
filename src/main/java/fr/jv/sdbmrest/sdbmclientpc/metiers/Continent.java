package fr.jv.sdbmrest.sdbmclientpc.metiers;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Continent {
    @NonNull
    @Schema(name="id", example = "3")
    private Integer id;
    @NonNull
    @Schema(name="libelle", example= "Europe")
    private String libelle;
    @Schema(name="pays", example = "['Allemagne', 'France']")
    private ArrayList<Pays> pays;

    public Continent(int id , @NonNull String libelle){
        this.id = id;
        this.libelle = libelle;
        pays = new ArrayList<>();


    }
    @Override
    public String toString(){
        return libelle;

    }

    public boolean equals(Object other){
        if(other == this )
            return true;
        if(!(other instanceof Continent ))
            return false;
        Continent continent = (Continent) other;
        return continent.id.equals(this.id);

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}