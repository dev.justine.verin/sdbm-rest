package fr.jv.sdbmrest.sdbmclientpc.dao;


import fr.jv.sdbmrest.sdbmclientpc.metiers.Continent;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Fabricant;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Marque;
import fr.jv.sdbmrest.sdbmclientpc.metiers.Pays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MarqueDAO extends DAO<Marque, Marque, Integer>{
    @Override
    public Marque getById(Integer id) {
        String sqlRequest = "SELECT ID_MARQUE, NOM_MARQUE, M.ID_FABRICANT, " +
                "NOM_FABRICANT,M.ID_PAYS, NOM_PAYS, P.ID_CONTINENT, NOM_CONTINENT from " +
                "MARQUE AS M " +
                " INNER JOIN FABRICANT AS F on M.ID_FABRICANT = F.ID_FABRICANT" +
                " INNER JOIN PAYS as P on M.ID_PAYS = P.ID_PAYS" +
                " INNER JOIN CONTINENT as C on C.ID_CONTINENT = P.ID_CONTINENT " +
                "WHERE ID_MARQUE = " + id;



        try(Statement statement = connexion.createStatement()){  //exemple try with
            // resources
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if(resultSet.next())
                return new Marque(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        new Fabricant(resultSet.getInt(3), resultSet.getString(4)),
                        new Pays(resultSet.getInt(5), resultSet.getString(6),
                                new Continent(resultSet.getInt(7),
                                        resultSet.getString(8))));
            return null;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public ArrayList<Marque> getAll() {
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_MARQUE, NOM_MARQUE, M.ID_FABRICANT, " +
                "NOM_FABRICANT,M.ID_PAYS, NOM_PAYS, P.ID_CONTINENT, NOM_CONTINENT from " +
                "MARQUE AS M " +
                " LEFT JOIN FABRICANT AS F on M.ID_FABRICANT = F.ID_FABRICANT" +
                " INNER JOIN PAYS as P on M.ID_PAYS = P.ID_PAYS" +
                " INNER JOIN CONTINENT as C on C.ID_CONTINENT = P.ID_CONTINENT ";

        try (Statement statement = connexion.createStatement()){
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            while(resultSet.next()){
                liste.add(new Marque(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        new Fabricant(resultSet.getInt(3), resultSet.getString(4)),
                        new Pays(resultSet.getInt(5), resultSet.getString(6),
                                new Continent(resultSet.getInt(7),
                                        resultSet.getString(8)))

                ));
            }
            resultSet.close();

        }
        catch(SQLException exception){
            exception.printStackTrace();
        }
        return liste;


    }

    @Override
    public ArrayList<Marque> getLike(Marque object) {
        return null;
    }

    public ArrayList<Marque> getByFabricant(Fabricant fabricant){
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlStatement = "SELECT ID_MARQUE, NOM_MARQUE, M.ID_FABRICANT, " +
                "NOM_FABRICANT,M.ID_PAYS, NOM_PAYS, P.ID_CONTINENT, NOM_CONTINENT from " +
                "MARQUE AS M " +
                " INNER JOIN FABRICANT AS F on M.ID_FABRICANT = F.ID_FABRICANT" +
                " INNER JOIN PAYS as P on M.ID_PAYS = P.ID_PAYS" +
                " INNER JOIN CONTINENT as C on C.ID_CONTINENT = P.ID_CONTINENT " +
                "WHERE M.ID_FABRICANT = ?";
        try(PreparedStatement preparedStatement = connexion.prepareStatement(sqlStatement)){
            preparedStatement.setInt(1, fabricant.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                liste.add(new Marque(resultSet.getInt("ID_MARQUE"),
                        resultSet.getString("NOM_MARQUE"), fabricant,
                        new Pays(resultSet.getInt("ID_PAYS"),
                                resultSet.getString("NOM_PAYS"),
                                new Continent(resultSet.getInt("ID_CONTINENT"),
                                        resultSet.getString("NOM_CONTINENT")
                                )
                        )
                )
                );

            }

        }
        catch(SQLException exception){
            exception.printStackTrace();
        }

        return liste;


    }

    @Override
    public boolean insert(Marque object) {
        return false;
    }

    @Override
    public boolean update(Marque object) {
        return false;
    }

    @Override
    public boolean delete(Marque object) {
        return false;
    }
}